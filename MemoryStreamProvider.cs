﻿using Aspose.Html.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Aspose.Html.Test
{
    internal class MemoryStreamProvider : ICreateStreamProvider
    {
        public List<MemoryStream> Streams { get; } = new List<MemoryStream>();

        public Stream GetStream(string name, string extension)
        {
            var result = new MemoryStream();
            Streams.Add(result);

            return result;
        }

        public Stream GetStream(string name, string extension, int page)
        {
            var result = new MemoryStream();
            Streams.Add(result);

            return result;
        }

        public void ReleaseStream(Stream stream)
        {
            //  Here You can release the stream filled with data and, for instance, flush it to the hard-drive
        }

        public void Dispose()
        {
            foreach (var stream in Streams)
                stream.Dispose();
        }
    }
}
