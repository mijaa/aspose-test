﻿using Aspose.Html.Converters;
using Aspose.Html.Drawing;
using Aspose.Html.Saving;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Image = Aspose.Html.Rendering.Image;

namespace Aspose.Html.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            // Instantiate the License object.
           // License htmlLicense = new License();
            // Apply a license using the embedded resource name.
           //htmlLicense.SetLicense("Aspose.HTML.lic");
                        
            var content = ReadFile(@"test.html");

            var images = GeneratePdfPreviewWithAsposeHtmlNew(content);
            Console.WriteLine(images.Count());
        }

        private static string ReadFile(string fileName)
        {
            return File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName));
        }

        private static IEnumerable<string> GeneratePdfPreviewWithAsposeHtmlNew(string content)
        {
            var topAndBottom = 45;
            var leftAndRight = 68;

            var a4Size = new Size(Unit.FromPixels(794), Unit.FromPixels(1123));

            var images = new List<string>();

                using (var streamProvider = new MemoryStreamProvider())
                {
                    using (var document = new HTMLDocument(content, "."))
                    {
                        var options = new ImageSaveOptions(Image.ImageFormat.Jpeg)
                        {
                            HorizontalResolution = 96,
                            VerticalResolution = 96,
                            PageSetup =
                        {
                            AnyPage = new Aspose.Html.Drawing.Page
                            {
                                Size = a4Size,
                                Margin = new Margin(leftAndRight, topAndBottom, leftAndRight, topAndBottom)
                            }
                        }
                        };

                        Converter.ConvertHTML(document, options, streamProvider);

                        images.AddRange(streamProvider
                            .Streams
                            .Select(image =>
                            {
                                //image = ImageHelper.AddDraftWatermark(image);

                                return Convert.ToBase64String(image.ToArray());
                            }));
                    }
                }

            return images;
        }
    }
}
